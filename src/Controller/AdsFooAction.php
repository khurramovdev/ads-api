<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\AdsRepository;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class AdsFooAction
{
    public function __invoke(AdsRepository $adsRepository)
    {
        return $adsRepository->findBy([], ['price' => 'desc']);
    }


}