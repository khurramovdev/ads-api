<?php

declare(strict_types=1);

namespace App\Component\User;

use Symfony\Component\Serializer\Annotation\Groups;

class FooDto
{
    public function __construct(
        #[Groups(['user:write', 'user:read'])]
        private string $givenName,

        #[Groups(['user:write', 'user:read'])]
        private string $familyName,

        #[Groups(['user:write'])]
        private int $age
    ) {

    }

    /**
     * @return string
     */
    public function getGivenName(): string
    {
        return $this->givenName;
    }

    /**
     * @return string
     */
    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

}