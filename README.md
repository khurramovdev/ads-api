# ads-api
* khurramovdev@gmail.com

Test api for vacancy

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This back-end project creates, processes, and deletes ads. So far, the test is a project and written for the vacancy.

## Technologies
Project is created with:
* Symfony version: 5.2.14
* Composer version: 2.0.12
* Api-platform version: 2.6.5
* Doctrine/doctrine-bundle version: 2.4.2
* Doctrine/doctrine-migrations-bundle version: 3.1.1
* Doctrine/orm version: 2.9.5
* Doctrine/annotations version: 1.13.2
* Lexik/jwt-authentication-bundle version: 2.12.6

## Setup
To run this project, install it locally using composer:

```
$ cd ads-api
$ composer install
$ php -S localhost:8888 -t public
$ Go to search in browser.
```
* [http://localhost:8888/api](http://localhost:8888/api)

